(function (factory) {
    let global = typeof globalThis !== 'undefined' ? globalThis :
        typeof self !== "undefined" ? self :
            typeof window !== "undefined" ? window :
                typeof global !== "undefined" ? global : {};
    if (typeof exports === 'object' && typeof module === 'object'){ //CommonJS
        module.exports = factory(global);
    }else { //全局变量
        global.ImgCode = factory(global);
    }
})(function (global) {
    let document = global?.document;
    if (!document) return console.error('缺少 document 对象！');

    var img_refresh = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAAAnFBMVEVHcEwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADKMLGMAAAAM3RSTlMAkWy+4jL66fQgetL+yVkMCEzEENcnULkCYBdFZXA1zkE7iRt0BLJUq4DbpaCF8CuMLZaUbptlAAABgUlEQVR42o2U2W7qMBBAJ6uzAglhLWvpApQu957//7cqaVBxQuqcF0ujI3vG47HoDKdW7gaB2r7ZE6mwXqXNIA/45fwVithsW5rt0uRtB3i69plTkkevu423z1aXACABLpq3SAC1/rxNdwaVPLrxIoDTUHReqFjoXryRBtk1099qAWfe9A4+NeM6EgK5NPFSrth1qICztBhbXIl/kl8BE7nDKPQGjzMXeJKSAN6lm3C6Pg7qSj6epQdbWPfxRgkc+ogZpNKH/z1PlhymZmsp4sLO6C38rQSwMYoFlKJnFB0QBXujmILEkBlFBXKEhbFmQGy4mMQNuBJCMDKIj2BV5z8YxKAq2ALnb+8B/LnIGNOWLpzK9QjuX14EVFWME7C6vUk9M/V02V3eQUFx+3WsOkbLBZZyJQaie56n0Fo8LM3Zv6Y2jwC0L/fZAXgPtdj0DPhZu03Ay2AyrPY67L8UQBq2Ox9TERTOLFUJJf7T/clN0VCnZefl2s4HFX5h7bRH9Q0mUkP8xAIcLgAAAABJRU5ErkJggg==';
    var img_point = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAABACAMAAABm4HQzAAAA8FBMVEXJAAD1zc3////QIiLMDQ3+9/fKBQVMaXH////SKir////////////////////////////42tr////PHBzdXl7////////////77e3++vr////zw8PaUVH////////////////////////88vLtpqb////rn5/jeXnYRkb////////////////////WPDz76ur////faGj65+f////wuLjnjo788vLvsbHqnJzxvb3////////hcXH////so6P////209P////////////////////miYn////lhIT////////44OD21dXjfX3///////9/dYPfAAAAT3RSTlOA1QmJg/eBAP6LBOjazQGO9N9JiJ2XYOHu+1LPmELAndUR+/S+prqolO8xaxgdkex8oeqvyLHzw7jLeTykKbzu2nUftexYrzetIX7k3Km0zyANmAAAAqtJREFUeNqV1ulW4jAYgOEApQllR3ZkVZTNBWEQt3EdR2d77/9uptSjgpSSvP+f0zTpST8hfboZNhulDGRKjebwRvq0xlL1ZpyV4s16agtzigl8ShSdIFYtARANza97EcuK9K7noSgApepGdvTomYu8LZay81NPPj74s2p/gfZ3xFo7+wvYr/qxUwXqwkM+8EKB+pX6ylIFoJwXG8uXgULqC0sD2YgIKJIF0qvsEghZIjArBFwus5xylS22ZLtO5T7ZKA5ZS2zNykJ89MEmMIsJjWIzmLyzHKix0GqsIPfGnATUhGY1SDgeK0I0pstiUSh6LAnnQrt9SC7YACoRfRapwMBlaagJg2qQdlkJxiZsDCUpXiFqmzA7Cq+iCHfCqDsoigJ8N2PfoSAacGjGDqEhEtAzYz1IiFuImLEIxIUCYZYNmQWzDB0o0YeY8SL7ogW7ZmwXWmICL2bsBSbiFKZmbAqX4gpOzNgZ1IXzZPhyx5AJC1mAcxO2D10p5BXMLH1ll6HqslQCno0+5H7YZXIIZ/osC2m5YOEn+Kur8tB59Zg8hbKl/7CufGPhOMz11DV0bjzmNtS9l60TaMp35rQgpHlmmdEHk1dK60Y5rsD98k+4AHtbl2lnoeUss3ZJ47o8AHW1OinU1dbd3K2sTgpeaajkg9TOCbTCX1k4ueUHHoLOYH14eniC31bQFc7Qb1TLdQJO77ADXf/B8B4437wdybA/kwXgwPecZ1A6khuY0wD1bV3FypAZyE3M287OmoudQScXNGK3W97z1tWP4IF+tHAHK+914qqiDGaynQSm9ucezrxnBTLPNYA/7zPzcwUyVbmdyXD3Y2zeqQHxn1KHydRpB1StF5vvAckjqcG86nF4S6Udqc1ku6kASLoL1GVeg+5t5l8uJf37D5GCo9KapmQiAAAAAElFTkSuQmCC';
    var className = 'ImgCode'+(new Date()).getTime();

    var html = '<style type="text/css">@keyframes turn'+className+'{0%{transform: rotate(0deg);}100%{transform: rotate(360deg);}}';
    html += '.'+className+'{position: fixed;color: #000;left: 0;top: 0;width: 100%;height: 100%;z-index: 999999;display: flex;justify-content: center;align-items: center;}';
    html += '.'+className+' *{box-sizing: border-box;margin: 0;padding: 0;-webkit-tap-highlight-color: rgba(0,0,0,0);}';
    html += '.'+className+'>p{position: absolute;left: 0;top: 0;width: 100%;height: 100%;z-index: 0;}';
    html += '.'+className+'>div{width: 6em;background: #fff;padding: 0.4em;border-radius: 0.08em;position: relative;}';
    html += '.'+className+'>div>header{display: flex;width: 100%;height: 0.6em;font-weight: bold;position: relative;justify-content: center;align-items: center;}';
    html += '.'+className+'>div>header>span{font-size: 0.32em;}';
    html += '.'+className+'>div>header>img{position: absolute;width: 0.4em;right: 0.1em;top: 0.1em;}';
    html += '.'+className+'>div>header>img.runing{animation: turn'+className+' 1s linear 0s infinite;}';
    html += '.'+className+'>div>div{width: 100%;background: #e7e7e7;position: relative;line-height: 0;}';
    html += '.'+className+'>div>div>img{position: relative;width: 100%;z-index: 0;}';
    html += '.'+className+'>div>div>p{position: absolute;z-index: 2;width: 0.54em;height: 0.64em;text-align: center;transform: translate(-50%, -100%);background: url('+img_point+');background-size: 100%;}';
    html += '.'+className+'>div>div>p>span{font-size: 0.3em;line-height: 2;color: #fff;}';
    html += '.'+className+'>div>footer{height: 0.6em;margin-top: 0.1em;display: flex;justify-content: space-between;align-items: center;}';
    html += '.'+className+'>div>footer>span{font-size: 0.28em;text-align: right;flex-basis: 100%;}';
    html += '.'+className+'>div>footer>img{width: auto;height: 100%;margin-left: 0.2em;}</style><p onclick="this.parentNode.style.display=\'none\'"></p>';
    html += '<div><header><span>安全验证</span><img src="'+img_refresh+'"/></header><div class="__body__"><img src=""/></div><footer><span>点击图片中的:</span><img src=""/></footer></div>';

    /**
     * 图片验证码
     * @param {Object|Function} op 配置参数，如果传入 Function ，等效于 op.img_data。
     * @param {Function} op.img_data 返回验证码图片地址(格式：{map:点击背景图, target:表示点击目标的图片})的方法，异步情况返回 Promise
     * @param {Function} op.size 为了移动端适配，可以自定义图片验证码DOM的尺寸，单位(px)，显示的dom宽度为：6*size，默认：100
     * @param {Function} op.onclick 点击图片验证码的回调函数，接受一个对象(格式：{left:点击位置距左比例[0-1], top:点击位置距上比例[0-1]})数组，
     * @returns {ImgCode} 图片验证码对象，
     * ImgCode.show(onclick) 显示，可以传入点击后的回调函数，等下配置参数 op.onclick；
     * ImgCode.hide() 隐藏图片验证码视图；
     * ImgCode.refresh() 刷新图片验证码，会重新调用 op.img_data 方法
     * @constructor
     */
    function ImgCode(op){
        if(!(this instanceof ImgCode)) return new ImgCode(op);
        op = typeof op === 'function' ? {img_data: op} : (op || {});
        if (typeof op.img_data !== 'function') throw new Error("img_data 配置必须为函数");
        this.img_data = op.img_data;
        this.points=[];
        this.container = document.createElement('div');
        this.container.innerHTML = html;
        this.container.style.display = 'none';
        this.container.style.fontSize = (op.size || 100)+'px';
        this.container.className = className;
        this.onclick = op.onclick;

        let imgcode = this;
        this.container.querySelector('.__body__').addEventListener('click', function(e){
            let exy = {x: e.clientX, y: e.clientY};
            let r =  dom.getBoundingClientRect();
            let dxy = {x:r.left ,y:r.top};
            let xy = {x: exy.x-dxy.x, y: exy.y-dxy.y};

            let w = dom.offsetWidth;
            let h = dom.offsetHeight;
            let p = {left: xy.x/w, top: xy.y/h};
            imgcode.points.push(p);

            this.innerHTML += '<p style="left:'+(p.left*100)+'%; top:'+(p.top*100)+'%;"><span>'+imgcode.points.length+'</span></p>';
            if (typeof imgcode.onclick === 'function') imgcode.onclick.call(imgcode, imgcode.points);
        })
        this.container.querySelector('header>img').addEventListener('click', function(){ imgcode.refresh() });
        (op.dom || document.documentElement).appendChild(this.container);
        this.refresh();
    }
    ImgCode.prototype.show = function (onclick){
        if (typeof onclick == 'function') this.onclick = onclick;
        this.container.style.display = 'flex';
    }
    ImgCode.prototype.hide = function (){
        this.container.style.display = 'none';
    }
    ImgCode.prototype.refresh = function (){
        if(this.runing) return;
        this.runing = true;
        this.container.querySelector('header>img').className='runing';
        this.points=[];
        this.container.querySelectorAll('.__body__>p').forEach(function (dom){
            dom.parentNode.removeChild(dom);
        })
        Promise.resolve(this.img_data()).then((data)=>{
            if (!data?.map || !data?.target) throw '参数错误';
            imgcode.container.querySelector('.__body__>img').src = data.map;
            imgcode.container.querySelector('footer>img').src = data.target;
            imgcode.runing = false;
            imgcode.container.querySelector('header>img').className='';
        }).catch(()=>{ this.runing = false; })
    }

    return ImgCode;
});